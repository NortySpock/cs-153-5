//////////////////////////////////////////////////////////////////////////////
/// @file bst.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the header file for the Binary Search Tree class
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class Bst
/// @brief Creates an binary search tree. 
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn Bst()
/// @brief The constructor for the binary search tree
/// @pre No binary seach tree exists
/// @post A binary search tree exists, of size zero and with root pointing at
/// null
/// @param None (default constructor)
/// @return Nothing (constructor)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn ~Bst()
/// @brief The destructor for the binary search tree
/// @pre Binary search tree exists
/// @post Binary search tree does not exist
/// @param None (destructor)
/// @return None (destructor)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void insert (generic)
/// @brief Inserts an element into the BST if it doesn't exist already.
/// (If it does, the function does nothing.)
/// @pre BST exists
/// @post BST has one more element in it, and size is increased by one
/// @param Item to be added
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void remove (generic)
/// @brief Removes an element from the BST if it exists, else does nothing
/// @pre BST exists
/// @post BST does not contain the element specified, size is decreased by one
/// @param Element to be removed
/// @return Nothing (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn generic & search(generic)
/// @brief Allows you to check to see if an element exists in the tree.
/// If the tree is empty, throws CONTAINER_EMPTY exception
/// If the element is not in the tree, throws a ITEM_NOT_FOUND exception
/// If the element IS in the tree, returns the element
/// @pre Tree exists
/// @post Returns element in the tree or throws exception
/// @param Element to be found
/// @return Element
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn Btn<generic> * searchptr(generic)
/// @brief Returns a pointer to the requested node
/// If the tree is empty, throws CONTAINER_EMPTY exception
/// If the element is not in the tree, returns a null pointer
/// If the element is in the tree, returns a pointer to the element.
/// @pre Tree exists
/// @post No change to tree
/// @param The element you want a pointer to
/// @return Pointer to node with element.
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void clear ()
/// @brief Clears out everything in the tree
/// @pre Tree has elements in it
/// @post Tree has no elements in it, size is zero, and root points to null
/// @param None 
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn bool empty ()
/// @brief Returns whether or not the tree is empty 
/// (It does this by checking if m_root is null)
/// @pre Tree exists
/// @post No change
/// @param None
/// @return True if tree is empty, false if tree is not.
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size ()
/// @brief Returns the current number of elements in the tree
/// @pre Tree exists
/// @post No change
/// @param None
/// @return Number of elements in the tree.
////////////////////////////////////////////////////////////////////////////// 


#ifndef BST_H
#define BST_H

#include "btn.h"
#include "exception.h"
template <class generic>
class Bst
{
  public:
    Bst();
    ~Bst();
    void insert (generic);
    void remove (generic);
    generic & search(generic);
    Btn<generic> * searchptr(generic);
    void clear();
    bool empty();
    unsigned int size();
    
  private:
    unsigned int m_size; //Current size of the array
    Btn<generic> * m_root; //The pointer to the root node
};

#include "bst.hpp"
#endif
