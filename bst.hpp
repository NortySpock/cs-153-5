//////////////////////////////////////////////////////////////////////////////
/// @file bst.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the implementation file for the Binary Search Tree
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn Bst()
/// @brief The constructor for the binary search tree
/// @pre No binary seach tree exists
/// @post A binary search tree exists, of size zero and with root pointing at
/// null/
/// @param None (default constructor)
/// @return Nothing (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
Bst<generic>::Bst()
{
  m_size = 0;
  m_root = NULL;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn ~Bst()
/// @brief The destructor for the binary search tree
/// @pre Binary search tree exists
/// @post Binary search tree does not exist
/// @param None (destructor)
/// @return None (destructor)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
Bst<generic>::~Bst ()
{
  clear();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void insert (generic)
/// @brief Inserts an element into the BST if it doesn't exist already.
/// (If it does, the function does nothing.)
/// @pre BST exists
/// @post BST has one more element in it, and size is increased by one
/// @param Item to be added
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void Bst<generic>::insert(generic x)
{
  if(empty())//First post!
  {
    Btn<generic> * temp = new Btn<generic>;
    temp -> data = new generic;
    m_root = temp;//Point the root at the new node.
    temp -> p = NULL; //No parent
    
    //No children
    temp -> l = NULL; 
    temp -> r = NULL;
    
    //Data
    *(temp -> data) = x; //Put the data in the data pointer
    
    m_size++;
  }
  else
  {
    //Try to find the item
    Btn<generic> * temp1 = searchptr(x);
    if(temp1 == NULL)//It doesn't exist yet
    {
      Btn<generic> * temp2 = m_root;//Start at the top
      bool searching = true;//We're still searching
      
      //Search for the future parent
      while(searching)
      {
        if(x < *(temp2 -> data))//We need to go left
        {
          if(temp2 -> l != NULL)//We can go left
          {
            temp2 = temp2 -> l;//Go left
          }
          else//We cannot go left
          {
            searching = false;//We're done searching
          }
        }
        else//We need to go right
        {
          if(temp2 -> r != NULL)//We can go right
          {
            temp2 = temp2 -> r;//Go right
          }
          else//We cannot go right
          {
            searching = false;//We're done searching
          }//else can't go right
        }//else don't need to go right
      }//while searching
      


      //Ok, so now temp2 pointing at the parent of where the child needs to go.
      //Create the child
      if(x < *(temp2 -> data))//We need to go left
      {
        
        temp2 -> l = new Btn<generic>; //Make a new node to the left
        temp2 -> l -> data = new generic;

        temp2 -> l -> p = temp2; //Point the child back at the parent
        temp2 -> l -> l = NULL; //Point child left at NULL
        temp2 -> l -> r = NULL; //Point child right at NULL
        
        *(temp2 -> l -> data) = x; //Put the data in the child's ponter
      }
      else//We need to go right
      {
        
        temp2 -> r = new Btn<generic>; //Make a new node to the right
        temp2 -> r -> data = new generic;

        
        temp2 -> r -> p = temp2; //Point the child back at the parent
        temp2 -> r -> l = NULL; //Point child left at NULL
        temp2 -> r -> r = NULL; //Point child right at NULL
        
        *(temp2 -> r -> data) = x; //Put the data in the child's pointer

      }
      m_size++;//We created a node, so size went up
    }
    else//It does exist
    {
      //Do nothing
    }//Else it exists
  }//else inserting something other than root
}//insert(generic x)

//////////////////////////////////////////////////////////////////////////////
/// @fn void remove (generic)
/// @brief Removes an element from the BST if it exists, else does nothing
/// @pre BST exists
/// @post BST does not contain the element specified, size is decreased by one
/// @param Element to be removed
/// @return Nothing (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void Bst<generic>::remove(generic x)
{

  Btn<generic> * deleteNode = searchptr(x);
  if(deleteNode == NULL)//The node we're looking for does not exist
  {
    //Do Nothing
  }
  else//The node we're looking for does exist
  {
    //Case one -- Both children are null
    if(deleteNode -> l == NULL && deleteNode -> r == NULL)
    {
      if(deleteNode -> p == NULL)//We're at root
      {
        //Set root to null, since we're done
        m_root = NULL;
        delete deleteNode -> data;
        delete deleteNode;
      }
      else//We're not at root
      {
        if(deleteNode -> p -> l == deleteNode)//We're on the left side.
        {
          deleteNode -> p -> l = NULL;
        }
        else//We're on the right side.
        {
          deleteNode -> p -> r = NULL;
        }        
        delete deleteNode -> data;
        delete deleteNode;
      }
    }
    //Case two -- node to delete has only one child
    else if((deleteNode -> l != NULL && deleteNode -> r == NULL) || (deleteNode -> l == NULL && deleteNode -> r != NULL))
    {
      
      if(deleteNode -> p == NULL)//We're at root
      {
        
        Btn<generic> * newRoot = deleteNode;
        if(deleteNode -> l != NULL)//Next node down is on left side
        {
          newRoot = deleteNode -> l;//Set newRoot to be left
          newRoot -> p = NULL;//Set the newRoot parent to null
          m_root = newRoot;//Set root to point to new root
          delete deleteNode -> data;
          delete deleteNode;
        }
        else//Next node down is on right side
        {
          newRoot = deleteNode -> r;//Set newRoot to be left
          newRoot -> p = NULL;//Set the newRoot parent to null
          m_root = newRoot;//Set root to point to new root
          delete deleteNode -> data;
          delete deleteNode;
        }
      }
      else//We're not at root
      {
        Btn<generic> * newParent = deleteNode;
        Btn<generic> * newChild = deleteNode;
        if(deleteNode -> l != NULL)//Next node down is on left side
        {
          if(*(deleteNode -> data) < *(deleteNode -> p ->data))//We're on left side of parent
          {
            //Set future parent and child
            newParent = deleteNode -> p;
            newChild = deleteNode -> l;
            
            //Point parent and child at each other
            newParent -> l = newChild;
            newChild -> p = newParent;            
            
            delete deleteNode -> data;
            delete deleteNode;
          }
          else//We're on right side of parent
          {
            //Set future parent and child
            newParent = deleteNode -> p;
            newChild = deleteNode -> l;
            
            //Point parent and child at each other
            newParent -> r = newChild;
            newChild -> p = newParent;            
            
            delete deleteNode -> data;
            delete deleteNode;
          }
        }
        else//Next node down is on right side
        {
          if(*(deleteNode -> data) < *(deleteNode -> p ->data))//We're on left side of parent
          {
            //Set future parent and child
            newParent = deleteNode -> p;
            newChild = deleteNode -> r;
            
            //Point parent and child at each other
            newParent -> l = newChild;
            newChild -> p = newParent;            
            
            delete deleteNode -> data;
            delete deleteNode;
          }
          else//We're on right side of parent
          {
            //Set future parent and child
            newParent = deleteNode -> p;
            newChild = deleteNode -> r;
            
            //Point parent and child at each other
            newParent -> r = newChild;
            newChild -> p = newParent;            
            
            delete deleteNode -> data;
            delete deleteNode;
          }//Else right side of parent
        }//Else node down is on right side
      }//We're not at root
    }//Only one child
    
    //Has two children
    else if(deleteNode -> l != NULL && deleteNode -> r != NULL)
    {
      
      //Case 4 -- has replacement node with no right node
      if(deleteNode -> l -> r == NULL)
      {
        Btn<generic> * swapNode = deleteNode -> l;
        
        *(deleteNode -> data) = *(swapNode -> data);
        
        //Swapnode has a child
        if(swapNode -> l != NULL)
        {
          deleteNode -> l = swapNode -> l;
          swapNode -> l -> p = deleteNode;
        }
        else//No child
        {
          deleteNode -> l = NULL;
        }
        
        delete swapNode -> data;
        delete swapNode;
        
      }
      //Case 3 and 5 -- can go right from left
      else
      {
        Btn<generic> * swapNode = deleteNode -> l;
        //So long as we can go right
        while(swapNode -> r != NULL)
        {
          //Go right
          swapNode = swapNode -> r;
        }
        //Case 5
        if(swapNode -> l != NULL)
        {
          Btn<generic> * newParent = swapNode;
          Btn<generic> * newChild = swapNode;
          
          newChild = swapNode -> l;
          newParent = swapNode -> p;
          
          if(newParent -> l != NULL)
          {
            newParent = newParent -> l;
            while(newParent -> r != NULL)
            {
              newParent = newParent -> r;
            }
            //delete deleteNode -> data;
            *(deleteNode -> data) = *(swapNode -> data);
          
            //Point parent at child
            newParent -> r = newChild;
            newChild -> p = newParent;//point child at parent
          
          
            swapNode -> p -> r = NULL; 
          
            delete swapNode -> data;
            delete swapNode;
          }
          else
          {
          //delete deleteNode -> data;
          *(deleteNode -> data) = *(swapNode -> data);
          
          //Point parent at child
          newParent -> r = newChild;
          newChild -> p = newParent;//point child at parent
          
          delete swapNode -> data;
          delete swapNode;
          }
        }
        //Case 3
        else
        {
          //delete deleteNode -> data;
          *(deleteNode -> data) = *(swapNode -> data);
          
          swapNode -> p -> r = NULL;
          
          delete swapNode -> data;
          delete swapNode;
        }
      }
    }
    else//MAJOR ERROR
    {
      std::cerr<<"\n\n MAJOR ERROR \n\n";
    }
    m_size--;
  }
}//Remove

//////////////////////////////////////////////////////////////////////////////
/// @fn generic & search(generic)
/// @brief Allows you to check to see if an element exists in the tree.
/// If the tree is empty, throws an exception
/// If the element is not in the tree, throws a different exception
/// If the element IS in the tree, returns the element
/// @pre Tree exists
/// @post Returns element in the tree or throws exception
/// @param Element to be found
/// @return Element
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
generic & Bst<generic>::search(generic x)
{
  //If it's empty, throw exception
  if(empty())
  {
    throw Exception(CONTAINER_EMPTY, "The tree is empty.");
  }
  else
  {
    bool exists = true;//Assume it exists
    Btn<generic> * temp = m_root; //Make a temp and point it at the root
    
    //Now, so long as we think it exists and we haven't found it
    while(exists && *(temp -> data) != x)
    {
      if(x < *(temp -> data))//Need to go left
      {
        if(temp -> l != NULL)//We can go left
        {
          temp = temp -> l;//Go left
        }
        else//We cannot go left
        {
          exists = false;//The item does not exist
        }
      }
      else//Need to go right
      {
        if(temp -> r != NULL)//We can go right
        {
          temp = temp -> r;//Go right
        }
        else//We cannot go right
        {
          exists = false;//The item does not exist
        }
      }
    }
    if(exists)//We still think it exists, thus we're pointing at it
    {
      return *temp->data;
    }
    else//We didn't find it
    {
      throw Exception(ITEM_NOT_FOUND, "The item you requested was not found.");
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
/// @fn Btn<generic> * searchptr(generic)
/// @brief Returns a pointer to the requested node
/// If the tree is empty, throws an exception
/// If the element is not in the tree, returns a null pointer
/// If the element is in the tree, returns a pointer to the element.
/// @pre Tree exists
/// @post No change to tree
/// @param The element you want a pointer to
/// @return Pointer to node with element.
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
Btn<generic> * Bst<generic>::searchptr(generic x)
{
  //If it's empty, throw exception
  if(empty())
  {
    throw Exception(CONTAINER_EMPTY, "The tree is empty.");
  }
  else
  {
    bool exists = true;//Assume it exists
    Btn<generic> * temp = m_root; //Make a temp and point it at the root
    
    //Now, so long as we think it exists and we haven't found it
    while(exists && *(temp -> data) != x)
    {
      if(x < *(temp -> data))//Need to go left
      {
        if(temp -> l != NULL)//We can go left
        {
          temp = temp -> l;//Go left
        }
        else//We cannot go left
        {
          exists = false;//The item does not exist
        }
      }
      else//Need to go right
      {
      
        if(temp -> r != NULL)//We can go right
        {
          temp = temp -> r;//Go right
        }
        else//We cannot go right
        {
          exists = false;//The item does not exist
        }
      }
    }
    if(exists)//We still think it exists, thus we're pointing at it
    {
      return temp;
    }
    else//We didn't find it
    {
      return NULL;
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void clear ()
/// @brief Clears out everything in the tree
/// @pre Tree has elements in it
/// @post Tree has no elements in it, size is zero, and root points to null
/// @param None 
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void Bst<generic>::clear()
{
  //So long as there's still something in the tree
  while(!empty())
  {
    //remove the node at the root... somehow. I don't care how, just do it!
    remove(*(m_root -> data));
  }
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool empty ()
/// @brief Returns whether or not the tree is empty 
/// (It does this by checking if m_root is null)
/// @pre Tree exists
/// @post No change
/// @param None
/// @return True if tree is empty, false if tree is not.
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
bool Bst<generic>::empty()
{
  if(m_root == NULL) //It's empty
  {
    return true;
  }
  else//It's not empty
  {
    return false;
  }
}


//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size ()
/// @brief Returns the current number of elements in the tree
/// @pre Tree exists
/// @post No change
/// @param None
/// @return Number of elements in the tree.
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
unsigned int Bst<generic>::size ()
{
  return m_size;
}
